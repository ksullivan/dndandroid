import localStorage from './localStorage';

class DataController {
    constructor() { }

    setItem(key, value) {
        return new Promise((resolve, reject) => {
            localStorage.add(key, value)
            .then(() => {
                resolve();
            })
            .catch(error => reject(error))
        })
    }

    getItem(key) {
        return new Promise((resolve, reject) => {
            localStorage.get(key)
            .then(result => {
                resolve(result);
            })
            .catch(error => reject(error))
        })
    }
}

const dataController = new DataController();
export default dataController;