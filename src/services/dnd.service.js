import apiService from './api.service';
import { Feature } from '../models/features';
import { Spell } from '../models/spells';

let DndService = class DndService {
    constructor() {
        this.page = '';
    }

    getSpells() {

        return new Promise((resolve, reject) => {
            fetch(apiService.getSpellApi())
                .then((response) => response.json())
                .then((response) => {
                    let spells = [];
                    i = 1;
                    response.results.forEach(element => {
                        spells.push(new Spell(i, element.name, element.url, null));
                        i++
                    });
                    resolve(spells);
                })
                .catch((error) => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getSpell(url) {
        return new Promise((resolve, reject) => {
            fetch(apiService.getUrl(url))
                .then((response) => response.json())
                .then((response) => {
                    description = "";
                    response.desc.forEach(line => {
                        description += line + '\n';
                    })
                    spell = new Spell(null, response.name, null, description);
                    resolve(spell);
                })
                .catch((error) => {
                    console.error(error);
                    reject(error);
                });
        });
    }


    getFeatures() {
        return new Promise((resolve, reject) => {
            fetch(apiService.getFeatureApi())
                .then((response) => response.json())
                .then((response) => {
                    let features = [];
                    i = 1;
                    response.results.forEach(element => {
                        features.push(new Feature(i, element.name, element.url, null, null));
                        i++;
                    });
                    resolve(features);
                })
                .catch((error) => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getFeature(url) {
        return new Promise((resolve, reject) => {
            fetch(apiService.getUrl(url))
                .then((response) => response.json())
                .then((response) => {
                    console.log("Name: ", response.name);
                    console.log("Level: ", response.level);
                    description = "";
                    response.desc.forEach(line => {
                        description += line + '\n';
                    })
                    feature = new Feature(null, response.name, null, description, response.level);
                    resolve(feature);
                })
                .catch((error) => {
                    console.error(error);
                    reject(error);
                });
        });
    }

};

// Create a Singleton
const dndService = new DndService();
export default dndService;