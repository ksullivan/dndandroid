let ApiService = class ApiService {
	constructor() {
		this.apiProtocol = 'http:';
		this.apiHost = '//dnd5eapi.co/api/';
		this.apiSpells = 'spells/';
		this.apiFeatures = 'features/';
	}

	/*
	* Utility methods/properties
	*/
	get apiLocation() {
		return `${this.apiProtocol}${this.apiHost}${this.apiPage}`;
	}

	getSpellApi() {
		return `${this.apiProtocol}${this.apiHost}${this.apiSpells}`;
	}

	getFeatureApi() {
		return `${this.apiProtocol}${this.apiHost}${this.apiFeatures}`;
	}

	getUrl(url) {
		return `${url}`;
	}

	/*
	* API addresses
	*/
	hitAPI() {
		return `${this.apiLocation}`;
	}
};

// Create a Singleton
const apiService = new ApiService();
export default apiService;