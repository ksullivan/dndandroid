/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { createAppContainer, createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import CharacterScreen from './screens/CharacterScreen';
import DiceScreen from './screens/DiceScreen';
import SpellScreen from './screens/SpellScreen';
import FeaturesScreen from './screens/FeaturesScreen';
import SelectedFeature from './screens/SelectedFeature';
import SelectedSpell from './screens/SelectedSpell';

import { StyleProvider } from 'native-base';
import getTheme from '../custom-theme/components';
import theme from '../custom-theme/variables/material';

export default class App extends Component {
  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
        <AppContainer />
      </StyleProvider>
    );
  }
}

const FeatureTab = createStackNavigator(
  {
    Features: FeaturesScreen,
    Feature: SelectedFeature,
  }
)

const SpellTab = createStackNavigator(
  {
    Spells: SpellScreen,
    Spell: SelectedSpell,

  }
)

const Root = createBottomTabNavigator(
  {
    Dice: DiceScreen,
    Character: CharacterScreen,
    Spells: SpellTab,
    Features: FeatureTab,
  },
  {
    tabBarOptions: {
      activeTintColor: theme.brandPrimary,
      labelStyle: { fontSize: 16, },
    }
  }
);

const AppContainer = createAppContainer(Root);


