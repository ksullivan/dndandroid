export class Spell {
    constructor(id, name, url, description) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.description = description;
    }

    getId() {
        return this.id;
    }

    getName() {
        return this.name;
    }

    getUrl() {
        return this.url
    }

    getDescription() {
        return this.description;
    }
}