export class Feature {
    constructor(id, name, url, description, level) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.description = description;
        this.level = level;
    }

    getId() {
        return this.id;
    }

    getName() {
        return this.name;
    }

    getUrl() {
        return this.url;
    }

    getDescription() {
        return this.description;
    }

    getLevel() {
        return this.level;
    }
}