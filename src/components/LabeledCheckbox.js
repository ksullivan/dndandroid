import React, { Component } from 'react';
import { CheckBox, ListItem, Text } from 'native-base';
import { View } from 'react-native';


export default class LabeledCheckbox extends Component
{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <ListItem onPress={()=>this.props.onPress()}>
                <CheckBox 
                    checked={this.props.checked} 
                    onPress={()=>this.props.onPress()}
                    style={{marginRight: 20}}
                />
                <Text>{this.props.text}</Text>
            </ListItem>
        )
    }
}