import React, { Component } from 'react';
import { Input, Text } from 'native-base';
import { View } from 'react-native';

export default class LabeledInput extends Component
{
    constructor(props) {
        super(props);
    }

    render(){
        return  (
        <View style={{ flexDirection: 'row', marginLeft:10}}>
            <Text style={{alignSelf: 'center'}}>{this.props.text}</Text>
            <Input
                defaultValue={this.props.defaultValue}
                onChangeText={this.props.onChangeText}
            ></Input>
        </View>
        )
    }

}