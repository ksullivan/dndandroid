import React, { Component, View, TouchableOpacity } from 'react';
import {Text} from 'native-base';

default export class Dice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            size: 1,
            displayValue: 1,
            value: 1,
            isRolling: false,
            counterMax: 90,
            counter: 0
        };
    }

    componentWillMount() {
        this.setState({size: this.props.size});
    }

    whileRolling() {
        if(this.state.isRolling && this.state.counter === 0) {
            this.setState({isRolling: false})
        }
        else if(this.state.isRolling) {
            this.setState((oldState) => {
                let newVal = 0;
                if(displayValue === size) {
                    newVal = 1;
                }
                else {
                    newVal = oldState.displayValue + 1;
                }
                return {
                    displayValue: newVal
                }
            })
        }
    }

    render() {
        this.whileRolling();
        return(
            <TouchableOpacity>
                <Text> {this.state.displayValue} </Text>
            </TouchableOpacity>
        )
    }
}