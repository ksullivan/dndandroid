import React, { Component } from 'react'
import { Container, Card, Content, Text } from 'native-base'
import LabeledInput  from '../components/LabeledInput'
import LabeledCheckbox from '../components/LabeledCheckbox'
import dataController from '../services/dataController'

export default class CharacterScreen extends Component
{
    constructor(props) {
        super(props);

        var base = {
            character: '-',
            class: '-',
            level: '1',
            background: '-',
            race: '-',
            alignment: '-',
            experience: '0',

            strength: '10',
            dexterity: '10',
            constitution: '10',
            intelligence: '10',
            wisdom: '10',
            charisma: '10',

            strengthChecked: false,
            dexterityChecked: false,
            constitutionChecked: false,
            intelligenceChecked: false,
            wisdomChecked: false,
            charismaChecked: false,

            acrobaticsChecked: false,
            animalHandlingChecked: false,
            aracanaChecked: false,
            athleticsChecked: false,
            deceptionChecked: false,
            historyChecked: false,
            insightChecked: false,
            initimidationChecked: false,
            investigationChecked: false,
            medicineChecked: false,
            natureChecked: false,
            perceptionChecked: false,
            performanceChecked: false,
            persuasionChecked: false,
            religionChecked: false,
            sleightChecked: false,
            stealthChecked: false,
            survivalChecked: false,
        }

        this.state = base; 
    }

    componentDidMount(){
        dataController.getItem('character').then((data) => {
            this.setState(data);
        }).catch(()=>{});
    }

    componentDidUpdate(){
        dataController.setItem('character', this.state).catch((r)=>{throw '' + r});
    }

    render() {
        return(
            <Container>
                <Content>
                    <Card>
                        <LabeledInput
                            text={'Character Name:'}
                            defaultValue={this.state.character}
                            onChangeText={(text)=>this.setState({character: text})}    
                        />
                        <LabeledInput
                            text={'Class:'}
                            defaultValue={this.state.class}
                            onChangeText={(text)=>this.setState({class: text})}    
                        />
                        <LabeledInput
                            text={'Level:'}
                            defaultValue={this.state.level}
                            onChangeText={(text)=>this.setState({level: text})}    
                        />
                        <LabeledInput
                            text={'Background:'}
                            defaultValue={this.state.background}
                            onChangeText={(text)=>this.setState({background: text})}    
                        />
                        <LabeledInput
                            text={'Race:'}
                            defaultValue={this.state.race}
                            onChangeText={(text)=>this.setState({race: text})}    
                        />
                        <LabeledInput
                            text={'Alignment:'}
                            defaultValue={this.state.alignment}
                            onChangeText={(text)=>this.setState({alignment: text})}    
                        />
                        <LabeledInput
                            text={'Experience:'}
                            defaultValue={this.state.experience}
                            onChangeText={(text)=>this.setState({experience: text})}    
                        />
                    </Card>
                    <Card>
                        <LabeledInput
                            text={'Strength:'}
                            defaultValue={this.state.strength}
                            onChangeText={(text)=>this.setState({strength: text})}    
                        />
                        <LabeledInput
                            text={'Dexterity:'}
                            defaultValue={this.state.dexterity}
                            onChangeText={(text)=>this.setState({dexterity: text})}    
                        />
                        <LabeledInput
                            text={'Constitution:'}
                            defaultValue={this.state.constitution}
                            onChangeText={(text)=>this.setState({constitution: text})}    
                        />
                        <LabeledInput
                            text={'Intelligence:'}
                            defaultValue={this.state.intelligence}
                            onChangeText={(text)=>this.setState({intelligence: text})}    
                        />
                        <LabeledInput
                            text={'Wisdom:'}
                            defaultValue={this.state.wisdom}
                            onChangeText={(text)=>this.setState({wisdom: text})}    
                        />
                        <LabeledInput
                            text={'Charisma:'}
                            defaultValue={this.state.charisma}
                            onChangeText={(text)=>this.setState({charisma: text})}    
                        />
                    </Card>
                    <Card>
                        <Text>{`Proficiency Bonus: ${ this.getProficiency() }`}</Text>
                    </Card>
                    <Card>
                        <Text>Saving Throws:</Text>
                        <LabeledCheckbox
                            text={`Strength: ${this.getModifier(this.state.strength) + (this.state.strengthChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.strengthChecked}
                            onPress={()=>this.setState(prev=>{return {strengthChecked: !prev.strengthChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Dexterity: ${this.getModifier(this.state.dexterity) + (this.state.dexterityChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.dexterityChecked}
                            onPress={()=>this.setState(prev=>{return {dexterityChecked: !prev.dexterityChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Constitution: ${this.getModifier(this.state.constitution) + (this.state.constitutionChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.constitutionChecked}
                            onPress={()=>this.setState(prev=>{return {constitutionChecked: !prev.constitutionChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Intelligence: ${this.getModifier(this.state.intelligence) + (this.state.intelligenceChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.intelligenceChecked}
                            onPress={()=>this.setState(prev=>{return {intelligenceChecked: !prev.intelligenceChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Wisdom: ${this.getModifier(this.state.wisdom) + (this.state.wisdomChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.wisdomChecked}
                            onPress={()=>this.setState(prev=>{return {wisdomChecked: !prev.wisdomChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Charisma: ${this.getModifier(this.state.charisma) + (this.state.charismaChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.charismaChecked}
                            onPress={()=>this.setState(prev=>{return {charismaChecked: !prev.charismaChecked} })}
                        />
                    </Card>
                    <Card>
                        <Text>Skills:</Text>
                        <LabeledCheckbox
                            text={`Acrobatics: ${this.getModifier(this.state.dexterity) + (this.state.acrobaticsChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.acrobaticsChecked}
                            onPress={()=>this.setState(prev=>{return {acrobaticsChecked: !prev.acrobaticsChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Animal Handling: ${this.getModifier(this.state.wisdom) + (this.state.animalHandlingChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.animalHandlingChecked}
                            onPress={()=>this.setState(prev=>{return {animalHandlingChecked: !prev.animalHandlingChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Arcana: ${this.getModifier(this.state.intelligence) + (this.state.aracanaChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.aracanaChecked}
                            onPress={()=>this.setState(prev=>{return {aracanaChecked: !prev.aracanaChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Athletics: ${this.getModifier(this.state.strength) + (this.state.athleticsChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.athleticsChecked}
                            onPress={()=>this.setState(prev=>{return {athleticsChecked: !prev.athleticsChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Deception: ${this.getModifier(this.state.charisma) + (this.state.deceptionChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.deceptionChecked}
                            onPress={()=>this.setState(prev=>{return {deceptionChecked: !prev.deceptionChecked} })}
                        />
                        <LabeledCheckbox
                            text={`History: ${this.getModifier(this.state.intelligence) + (this.state.historyChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.historyChecked}
                            onPress={()=>this.setState(prev=>{return {historyChecked: !prev.historyChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Insight: ${this.getModifier(this.state.wisdom) + (this.state.insightChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.insightChecked}
                            onPress={()=>this.setState(prev=>{return {insightChecked: !prev.insightChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Intimidation: ${this.getModifier(this.state.charisma) + (this.state.intimidationChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.intimidationChecked}
                            onPress={()=>this.setState(prev=>{return {intimidationChecked: !prev.intimidationChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Investigation: ${this.getModifier(this.state.intelligence) + (this.state.investigationChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.investigationChecked}
                            onPress={()=>this.setState(prev=>{return {investigationChecked: !prev.investigationChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Insight: ${this.getModifier(this.state.wisdom) + (this.state.medicineChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.medicineChecked}
                            onPress={()=>this.setState(prev=>{return {medicineChecked: !prev.medicineChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Nature: ${this.getModifier(this.state.intelligence) + (this.state.natureChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.natureChecked}
                            onPress={()=>this.setState(prev=>{return {natureChecked: !prev.natureChecked} })}
                        />  
                        <LabeledCheckbox
                            text={`Insight: ${this.getModifier(this.state.wisdom) + (this.state.perceptionChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.perceptionChecked}
                            onPress={()=>this.setState(prev=>{return {perceptionChecked: !prev.perceptionChecked} })}
                        />    
                        <LabeledCheckbox
                            text={`Performance: ${this.getModifier(this.state.charisma) + (this.state.performanceChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.performanceChecked}
                            onPress={()=>this.setState(prev=>{return {performanceChecked: !prev.performanceChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Persuasion: ${this.getModifier(this.state.charisma) + (this.state.persuasionChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.persuasionChecked}
                            onPress={()=>this.setState(prev=>{return {persuasionChecked: !prev.persuasionChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Religion: ${this.getModifier(this.state.intelligence) + (this.state.religionChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.religionChecked}
                            onPress={()=>this.setState(prev=>{return {religionChecked: !prev.religionChecked} })}
                        />  
                        <LabeledCheckbox
                            text={`Sleight of Hand: ${this.getModifier(this.state.dexterity) + (this.state.sleightChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.sleightChecked}
                            onPress={()=>this.setState(prev=>{return {sleightChecked: !prev.sleightChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Stealth: ${this.getModifier(this.state.dexterity) + (this.state.stealthChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.stealthChecked}
                            onPress={()=>this.setState(prev=>{return {stealthChecked: !prev.stealthChecked} })}
                        />
                        <LabeledCheckbox
                            text={`Survival: ${this.getModifier(this.state.wisdom) + (this.state.survivalChecked ? this.getProficiency() : 0)}`}
                            checked={this.state.survivalChecked}
                            onPress={()=>this.setState(prev=>{return {survivalChecked: !prev.survivalChecked} })}
                        />    
                    </Card>
                </Content>
            </Container>
        );
    }

    
    getProficiency(){
        return 2 + Math.floor((parseInt(this.state.level) - 1) / 4);
    }

    getModifier(val){
        return Math.floor((parseInt(val) - 10) / 2);
    }
}