import React, { Component } from 'react';
import { Image, ScrollView, FlatList } from 'react-native';
import { Body, Card, CardItem, Container, Content, Header, Title, Right, Text, Thumbnail, Left, Spinner, Button, Icon, Item, Input, List, ListItem } from 'native-base';

import dndService from '../services/dnd.service';

export default class SelectedFeature extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);

        this.state = {
            feature: { name: "", description: "", level: null },
            rendered: false
        }
    }

    componentDidMount() {
        const url = this.props.navigation.getParam('feature', 'feature not found');
        this.getFeature(url);
    }

    render() {

        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent>
                            <Icon name='arrow-back' onPress={() => this.props.navigation.goBack()} />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Feature Details</Title>
                    </Body>
                </Header>
                <Content>
                    <ScrollView>
                        <Card>
                            <CardItem header style={{ backgroundColor: '#530F10' }}>
                                <Text style={{ fontSize: 30 }}>{this.state.feature.name}</Text>
                            </CardItem>
                            <CardItem style={{ backgroundColor: '#530F10' }}>
                                <Text style={{ fontSize: 20 }}>Level: {this.state.feature.level}</Text>
                            </CardItem>
                            <CardItem style={{ backgroundColor: '#530F10' }}>
                                <Text style={{ fontSize: 20 }}>{this.state.feature.description}</Text>
                            </CardItem>
                        </Card>
                    </ScrollView>
                </Content>
            </Container>
        );
    }

    getFeature(url) {
        if (this.state.rendered) return;
        dndService.getFeature(url)
            .then(results => {
                console.log("Results: ", results);
                this.setState({ feature: results, rendered: true });
            })
            .catch(error => {
                console.log('Something went wrong in getFeature()!');
            })
    }

    renderEmptyList = () => {
        return (
            <Container>
                <Spinner color='#530F10' />
            </Container>
        )
    }
}