import React, { Component } from 'react';
import { Image, ScrollView, FlatList } from 'react-native';
import { Body, Card, CardItem, Container, Content, Header, Title, Right, Text, Thumbnail, Left, Spinner, Button, Icon, Item, Input, List, ListItem } from 'native-base';

import dndService from '../services/dnd.service';

export default class SelectedSpell extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);

        this.state = {
            spell: { name: "", description: "" },
            rendered: false
        }
    }

    componentDidMount() {
        const url = this.props.navigation.getParam('spell', 'spell not found');
        this.getSpell(url);
    }

    render() {

        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent>
                            <Icon name='arrow-back' onPress={() => this.props.navigation.goBack()} />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Spell Details</Title>
                    </Body>
                </Header>
                <Content>
                    <ScrollView>
                        <Card>
                            <CardItem header style={{ backgroundColor: '#530F10' }}>
                                <Text style={{ fontSize: 30 }}>{this.state.spell.name}</Text>
                            </CardItem>
                            <CardItem style={{ backgroundColor: '#530F10' }}>
                                <Text style={{ fontSize: 20 }}>{this.state.spell.description}</Text>
                            </CardItem>
                        </Card>
                    </ScrollView>
                </Content>
            </Container>
        );
    }

    getSpell(url) {
        if (this.state.rendered) return;
        dndService.getSpell(url)
            .then(results => {
                this.setState({ spell: results, rendered: true });
            })
            .catch(error => {
                console.log('Something went wrong in getSpell()!');
            })
    }

    renderEmptyList = () => {
        return (
            <Container>
                <Spinner color='#530F10' />
            </Container>
        )
    }
}