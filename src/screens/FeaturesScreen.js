import React, { Component } from 'react';
import { FlatList, Image, TouchableOpacity, TextInput, View } from 'react-native';
import { Body, Card, CardItem, Container, Content, Header, Title, Right, Text, Thumbnail, Left, Spinner, Button, Icon, Item, Input } from 'native-base';

import dndService from '../services/dnd.service';

export default class FeatureScreen extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);

        this.state = {
            features: [],
            page: "features/",
        }
    }

    componentWillMount() {
        this.getFeatures();
    }

    render() {
        return (
            <View style={{ backgroundColor: 'grey' }}>
                <View>
                    <Header>
                        <Body>
                            <Title>Features</Title>
                        </Body>
                    </Header>
                </View>
                <View>
                    <FlatList
                        data={this.state.features}
                        keyExtractor={(item, index) => item.id.toString()}
                        renderItem={this.renderFeatures}
                        contentContainerStyle={{ flexGrow: 1 }}
                        initialNumToRender={10}
                        onEndReachedThreshold={0.8}
                        onEndReached={this.getMovies}
                        maxToRenderPerBatch={10}
                        ListEmptyComponent={this.renderEmptyList}
                    >
                    </FlatList>
                </View>
            </View>
        );
    }

    renderFeatures = ({ item }) => {
        return (
            <TouchableOpacity style={{ margin: 0, padding: 0 }} onPress={() => { this.props.navigation.push('Feature', { feature: item.url }) }}>
                <Card>
                    <CardItem style={{ backgroundColor: '#530F10' }}>
                        <Text style={{ fontSize: 20 }}>{item.getName()}</Text>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        );
    }

    renderEmptyList = () => {
        return (
            <Container>
                <Spinner color='#530F10' />
            </Container>
        )
    }

    getFeatures = () => {
        dndService.getFeatures()
            .then(results => {
                let features = results;
                this.state.page = this.state.page + 1;
                features.forEach((element) => {
                    this.state.features.push(element);
                });
                this.setState();
            })
            .catch(error => {
                console.log('Something went wrong!');
            })
    }
}